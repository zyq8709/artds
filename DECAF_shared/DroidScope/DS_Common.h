/**
 * Copyright (C) <2011> <Syracuse System Security (Sycure) Lab>
 *
 * This library is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Lok Yan
 * @date 9/28/2011
 */

#ifndef DS_COMMON_H
#define DS_COMMON_H

//#include <inttypes.h>
//#include <stdio.h>
//#include "cpu.h"
#include "DECAF_shared/DECAF_types.h"

typedef enum {
  LOG_LEVEL_MINIMAL,
  LOG_LEVEL_SIMPLE,
  LOG_LEVEL_VERBOSE,
  LOG_LEVEL_EVERYTHING
} LogLevel;

/**
 * Strips the last bit of the link-register.
 * Remember that if bit 1 is 1 then the previous instruction was thumb, else it is not
 */
#define lp_strip(_lp) (_lp & ~0x1)

//There is some documentation that says that the PGDs for ARM must be 16k aligned - but we will just assume that they are 4k aligned and use that for stripping
//#define pgd_strip(_pgd) (_pgd & ~0xC0000000)
#define pgd_strip(_pgd) (_pgd & ~0xC0000FFF)

/**
 * Linux uses 4K pages but QEMU for ARM is setup to use 1k pages.
 */
#define LINUX_PAGE_BITS 12
#define LINUX_PAGE_SIZE ( 1 << LINUX_PAGE_BITS )
#define LINUX_OFFSET_MASK ( LINUX_PAGE_SIZE - 1 )
#define LINUX_PAGE_MASK ( ~LINUX_OFFSET_MASK )

#define SET_TASK_COMM_ADDR 0xc00b5314 //0xc0091694 set_task_comm
#define DO_FORK_ADDR 0xc0018120 //0xc0026f7c   do_fork
#define DO_EXECVE_ADDR 0xc00b6060 //0xc0092530 do_execve
//#define DO_MMAP2_ADDR 0xc000e278 //0xc0027030 sys_mmap2
#define DO_MMAP 0xc00a0a6c //0xc0027030 do_mmap
#define DO_PRCTL_ADDR 0xc002cb3c //0xc004939c sys_prctl
#define DO_CLONE_ADDR 0xc00108f4 //0xc0026f40 sys_clone
#define SWITCH_TO 0xc000da24 //__switch_to
#define DO_FORK_END_ADDR 0xc00184bc






#endif//DS_COMMON_H

/**
 * Created By Chenxiong Qian
 * date: 2014-12-3
 */
#include "framework_sources.h"
#include <map>
#include "DECAF_shared/utils/OutputWrapper.h"
#include "../object.h"

#define GENERAL_TAINT 0x00000001

int global_count=0;

using namespace std;


void fileHooker(CPUState* env, int afterInvoking){
    if (!afterInvoking) {
    }else{
        if (global_count>=32) {
            return;
        }
        insert(GENERAL_TAINT << global_count, env->regs[0]);
        DECAF_printf("Taint value: %x\n",GENERAL_TAINT<<global_count);
        DECAF_printf("Taint source No.%d\n",global_count);
        global_count++;
    }
}


void getidHooker(CPUState* env, int afterInvoking){
    if (!afterInvoking) {
    }else{
        if (global_count>=32) {
            return;
        }
        insert(GENERAL_TAINT<<global_count,env->regs[0]);
        DECAF_printf("Taint value: %x\n",GENERAL_TAINT<<global_count);
        DECAF_printf("Taint source No.%d\n",global_count);
        global_count++;
        
    }
}

void getLine1NumberHooker(CPUState* env, int afterInvoking){
    if (!afterInvoking) {
    }else{
        if (global_count>=32) {
            return;
        }
        insert(GENERAL_TAINT<<global_count,env->regs[0]);
        DECAF_printf("Taint value: %x\n",GENERAL_TAINT<<global_count);
        DECAF_printf("Taint source No.%d\n",global_count);
        global_count++;
    }
}

void tostrHooker(CPUState* env, int afterInvoking){
    if (!afterInvoking) {
    }else{
        if (global_count>=32) {
            return;
        }
        insert(GENERAL_TAINT<<global_count,env->regs[0]);
        DECAF_printf("Taint value: %x\n",GENERAL_TAINT<<global_count);
        DECAF_printf("Taint source No.%d\n",global_count);
        global_count++;
    }
}

void gettextHooker(CPUState* env, int afterInvoking){
    if (!afterInvoking) {
    }else{
        if (global_count>=32) {
            return;
        }
        insert(GENERAL_TAINT<<global_count,env->regs[0]);
        DECAF_printf("Taint value: %x\n",GENERAL_TAINT<<global_count);
        DECAF_printf("Taint source No.%d\n",global_count);
        global_count++;
    }
}

void gpsHooker(CPUState* env, int afterInvoking){
    if (!afterInvoking) {
    }else{
        if (global_count>=32) {
            return;
        }

        setRegTaint(0,GENERAL_TAINT<<global_count);
        setRegTaint(1,GENERAL_TAINT<<global_count);
        DECAF_printf("Taint value: %x\n",GENERAL_TAINT<<global_count);
        DECAF_printf("Taint source No.%d\n",global_count);
        global_count++;
    }
}


void gHooker(CPUState* env, int afterInvoking){
    if (!afterInvoking) {
    }else{
        if (global_count>=32) {
            return;
        }

        insert(GENERAL_TAINT<<global_count,env->regs[0]);
        DECAF_printf("Taint value: %x\n",GENERAL_TAINT<<global_count);
        DECAF_printf("Taint source No.%d\n",global_count);
        global_count++;
    }
}



struct cmp_str
{
    bool operator()(char const *a, char const *b)
    {
        return strcmp(a, b) < 0;
    }
};

std::map<char const*, frameworkCallHooker, cmp_str> sourceMethodHookerMap;

void frameworkSourceInit(){
    sourceMethodHookerMap.insert(std::pair<char const*, frameworkCallHooker>("java.lang.String android.telephony.TelephonyManager.getDeviceId()",
                                                                             getidHooker));  
    sourceMethodHookerMap.insert(std::pair<char const*, frameworkCallHooker>("java.lang.String android.telephony.TelephonyManager.getLine1Number()",
                                                                             getLine1NumberHooker));
    sourceMethodHookerMap.insert(std::pair<char const*, frameworkCallHooker>("java.io.FileInputStream android.content.ContextWrapper.openFileInput(java.lang.String)",
                                                                             fileHooker));
    sourceMethodHookerMap.insert(std::pair<char const*, frameworkCallHooker>("java.lang.String android.view.View.toString()",
                                                                             tostrHooker));
    sourceMethodHookerMap.insert(std::pair<char const*, frameworkCallHooker>("android.text.Editable android.widget.EditText.getText()",
                                                                             gettextHooker));
    sourceMethodHookerMap.insert(std::pair<char const*, frameworkCallHooker>("double android.location.Location.getLatitude()",
                                                                             gpsHooker));
    sourceMethodHookerMap.insert(std::pair<char const*, frameworkCallHooker>("double android.location.Location.getLongitude()",
                                                                             gpsHooker));
    sourceMethodHookerMap.insert(std::pair<char const*, frameworkCallHooker>("java.lang.String android.telephony.TelephonyManager.getSimSerialNumber()",
                                                                             getidHooker));
    sourceMethodHookerMap.insert(std::pair<char const*, frameworkCallHooker>("java.lang.String android.telephony.TelephonyManager.getSubscriberId()",
                                                                             getidHooker));
    sourceMethodHookerMap.insert(std::pair<char const*, frameworkCallHooker>("android.database.Cursor android.content.ContentResolver.query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String)",
                                                                             gHooker));

}

frameworkCallHooker hookSource(const char* methodName){
    std::map<char const*, frameworkCallHooker>::iterator it;
    it = sourceMethodHookerMap.find(methodName);
    if (it != sourceMethodHookerMap.end()){
        DECAF_printf("This is source!\n");
        return it->second;
    }
    return NULL;
}
